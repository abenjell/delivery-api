FROM amazoncorretto:21-alpine3.19

COPY /target/*.jar /app.jar

EXPOSE 8080

ENTRYPOINT ["java"]
CMD ["-jar", "/app.jar"]
