# Getting Started

## Build project
mvn clean package

## Run project 
mvn spring-boot:run

## Test the service
To check the service is UP visit http://localhost:8080/api/actuator/health

## Delivery Modes
http://localhost:8080/api/deliveryModes

## Slots management
use postman collection to test : delivery-api.postman_collection.json

## Api Doc 
http://localhost:8080/api/swagger-ui/index.html