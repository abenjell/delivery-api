package com.example.kata.delivery.controllers;

import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

public class AbstractController {

    /**
     * Get HttpHeaders with location
     * @param path
     * @param variables
     * @return
     */
    protected HttpHeaders getHeaders(String path, Object... variables) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(ServletUriComponentsBuilder.fromCurrentRequest().path(path).buildAndExpand(variables).toUri());
        return httpHeaders;
    }
}
