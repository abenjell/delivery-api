package com.example.kata.delivery.controllers;

import com.example.kata.delivery.models.ApiError;
import com.example.kata.delivery.models.DeliveryModeEnum;
import com.example.kata.delivery.models.Slot;
import com.example.kata.delivery.services.AppointmentService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
public class AppointmentController extends AbstractController {

    private AppointmentService appointmentService;
    @Operation(
            summary = "Get slots REST API",
            description = "Get list of available slots for the given delivery mode",
            tags = {"Slots"}
    )
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "HTTP Status 200 SUCCESS",
                    content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = Slot.class)))
            ),
            @ApiResponse(
                    responseCode = "500",
                    description = "HTTP Status 500 INTERNAL SERVER ERROR",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class))
            )
    })
    @GetMapping("/deliveryModes/{deliveryModeId}/slots")
    public ResponseEntity<List<Slot>> getAvailableSlots(@PathVariable DeliveryModeEnum deliveryModeId) {
        List<Slot> availableSlots = appointmentService.getAvailableSlots(deliveryModeId);
        return new ResponseEntity<>(availableSlots, HttpStatus.OK);
    }

    @Operation(
            summary = "Get slot REST API",
            description = "Get slot by slotId and deliveryMode",
            tags = {"Slots"}
    )
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "HTTP Status 200 SUCCESS",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Slot.class))
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "HTTP Status 404 NOT FOUND",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class))
            ),
            @ApiResponse(
                    responseCode = "500",
                    description = "HTTP Status 500 INTERNAL SERVER ERROR",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class))
            )
    })
    @GetMapping("/deliveryModes/{deliveryModeId}/slots/{slotId}")
    public ResponseEntity<Slot> getSlotById(@PathVariable DeliveryModeEnum deliveryModeId, @PathVariable String slotId) {
        Slot slot = appointmentService.getSlotById(deliveryModeId, slotId);
        return new ResponseEntity<>(slot, HttpStatus.OK);
    }

    @Operation(
            summary = "Create slot REST API",
            description = "Create Slot REST API is used to add new slot for the given delivery",
            tags = {"Slots"}
    )
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "HTTP Status 201 CREATED",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = Slot.class))
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "HTTP Status 400 BAD REQUEST",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class))
            ),
            @ApiResponse(
                    responseCode = "500",
                    description = "HTTP Status 500 INTERNAL SERVER ERROR",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiError.class))
            )
    })
    @PostMapping("/deliveryModes/{deliveryModeId}/slots")
    public ResponseEntity<Slot> createSlot(@PathVariable DeliveryModeEnum deliveryModeId, @Valid @RequestBody Slot slot) {
        Slot newSlot = appointmentService.createSlot(deliveryModeId, slot);
        HttpHeaders httpHeaders = getHeaders("/deliveryModes/{deliveryModeId}/slots/{slotId}", deliveryModeId, newSlot.getSlotId());
        return new ResponseEntity<>(newSlot, httpHeaders, HttpStatus.CREATED);
    }
}
