package com.example.kata.delivery.controllers;

import com.example.kata.delivery.models.DeliveryModeEnum;
import com.example.kata.delivery.services.DeliveryService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
@Tag(
        name = "DeliveryModes",
        description = "List all possible delivery mode"
)
public class DeliveryController {

    private DeliveryService deliveryService;

    @Operation(
            summary = "Get deliveryModes REST API",
            description = "Get list of available delivery modes"
    )
    @ApiResponse(
            responseCode = "200",
            description = "HTTP Status 200 SUCCESS"
    )
    @GetMapping("/deliveryModes")
    public ResponseEntity<List<DeliveryModeEnum>> getDeliveryModes() {
        List<DeliveryModeEnum> deliveryModes = deliveryService.getDeliveryModes();
        return  new ResponseEntity<>(deliveryModes, HttpStatus.OK);
    }

}
