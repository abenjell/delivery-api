package com.example.kata.delivery.services;

import com.example.kata.delivery.models.DeliveryModeEnum;

import java.util.List;

public interface DeliveryService {

    List<DeliveryModeEnum> getDeliveryModes();
}
