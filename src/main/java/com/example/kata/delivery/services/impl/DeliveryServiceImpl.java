package com.example.kata.delivery.services.impl;

import com.example.kata.delivery.models.DeliveryModeEnum;
import com.example.kata.delivery.services.DeliveryService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class DeliveryServiceImpl implements DeliveryService {
    @Override
    public List<DeliveryModeEnum> getDeliveryModes() {
        return  Stream.of(DeliveryModeEnum.values()).collect(Collectors.toList());
    }
}
