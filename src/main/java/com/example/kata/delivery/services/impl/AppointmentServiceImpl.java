package com.example.kata.delivery.services.impl;

import com.example.kata.delivery.exceptions.ResourceNotFoundException;
import com.example.kata.delivery.models.DeliveryModeEnum;
import com.example.kata.delivery.models.Slot;
import com.example.kata.delivery.services.AppointmentService;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class AppointmentServiceImpl implements AppointmentService {

    private List<Slot> slots = new ArrayList<>();

    @Override
    public List<Slot> getAvailableSlots(DeliveryModeEnum deliveryMode) {
        Stream<Slot> slotStream = slots.stream().filter(slot -> slot.getDeliveryModeEnum() == deliveryMode);
        if (deliveryMode == DeliveryModeEnum.DELIVERY_TODAY) {
            //keep only slots of the day
            slotStream = slotStream.filter(slot -> slot.getDay().atStartOfDay().equals(LocalDate.now().atStartOfDay()));
        }
        return slotStream.collect(Collectors.toList());
    }

    @Override
    public Slot createSlot(DeliveryModeEnum deliveryMode, Slot slot) {
        slot.setDeliveryModeEnum(deliveryMode);
        slot.setSlotId(Long.valueOf(slots.size() + 1));
        slots.add(slot);
        return slot;
    }

    @Override
    public Slot getSlotById(DeliveryModeEnum deliveryModeEnum, String slotId) {
        Optional<Slot> optionalSlot = Optional.ofNullable(slots.stream().filter(slot -> Objects.equals(slot.getSlotId(), Long.valueOf(slotId)))
                .findFirst().orElseThrow(() -> new ResourceNotFoundException(String.format("No slot exists with identifier %s and delivery Mode %s", slotId, deliveryModeEnum))));
        return optionalSlot.get();
    }
}
