package com.example.kata.delivery.services;

import com.example.kata.delivery.models.DeliveryModeEnum;
import com.example.kata.delivery.models.Slot;

import java.util.List;

public interface AppointmentService {

    List<Slot> getAvailableSlots(DeliveryModeEnum deliveryMode);
    Slot createSlot(DeliveryModeEnum deliveryMode, Slot slot);
    Slot getSlotById(DeliveryModeEnum deliveryModeEnum, String slotId);
}
