package com.example.kata.delivery.models;

import jakarta.validation.constraints.FutureOrPresent;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.sql.Time;
import java.time.LocalDate;

@Getter
@Setter
public class Slot {

    private Long slotId;
    @NotNull
    @FutureOrPresent
    private LocalDate day;
    @NotNull
    private Time startTime;
    @NotNull
    private Time endTime;
    private DeliveryModeEnum deliveryModeEnum;
}
