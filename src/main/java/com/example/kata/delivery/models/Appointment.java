package com.example.kata.delivery.models;


import java.sql.Timestamp;

public class Appointment {
    private long appointmentId;
    private Slot slot;
    private String bookedBy;
    private Timestamp createdAt;
}
