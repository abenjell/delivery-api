package com.example.kata.delivery.models;

public enum DeliveryModeEnum {
    DRIVE, DELIVERY, DELIVERY_TODAY, DELIVERY_ASAP;
}
