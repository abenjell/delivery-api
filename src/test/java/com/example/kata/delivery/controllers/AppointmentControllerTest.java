package com.example.kata.delivery.controllers;

import com.example.kata.delivery.models.DeliveryModeEnum;
import com.example.kata.delivery.models.Slot;
import com.example.kata.delivery.services.AppointmentService;
import jakarta.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.sql.Time;
import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AppointmentControllerTest {

    @InjectMocks
    private AppointmentController appointmentController;

    @Mock
    private AppointmentService appointmentService;

    @BeforeEach
    void setup() {
        ServletRequestAttributes servletRequestAttributes = mock(ServletRequestAttributes.class);
        HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
        RequestContextHolder.setRequestAttributes(servletRequestAttributes);
        when(servletRequestAttributes.getRequest()).thenReturn(httpServletRequest);
    }

    @Test
    void createSlotTest() {
        //GIVEN
        Slot slot = new Slot();
        slot.setDay(LocalDate.now().plusDays(1));
        slot.setStartTime(Time.valueOf("01:00:00"));
        slot.setEndTime(Time.valueOf("02:00:00"));
        slot.setSlotId(Long.valueOf(1));
        when(appointmentService.createSlot(any(DeliveryModeEnum.class), any(Slot.class))).thenReturn(slot);
        //WHEN
        ResponseEntity<Slot> slotCreated = appointmentController.createSlot(DeliveryModeEnum.DELIVERY, slot);
        //THEN
        assertThat(slotCreated).isNotNull();
        assertThat(slotCreated.getHeaders()).isNotNull();
        assertThat(slotCreated.getHeaders().getLocation()).isEqualTo(ServletUriComponentsBuilder.fromCurrentRequest().path("/deliveryModes/{deliveryModeId}/slots/{slotId}").buildAndExpand(DeliveryModeEnum.DELIVERY, slot.getSlotId()).toUri());
        assertThat(slotCreated.getBody().getDay()).isEqualTo(slot.getDay());
        assertThat(slotCreated.getBody().getStartTime()).isEqualTo(slot.getStartTime());
        assertThat(slotCreated.getBody().getEndTime()).isEqualTo(slot.getEndTime());
    }
}
