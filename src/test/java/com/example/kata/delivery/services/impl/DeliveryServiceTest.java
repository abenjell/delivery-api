package com.example.kata.delivery.services.impl;

import com.example.kata.delivery.models.DeliveryModeEnum;
import com.example.kata.delivery.services.DeliveryService;
import org.junit.jupiter.api.Test;

import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;

public class DeliveryServiceTest {

    private DeliveryService deliveryService = new DeliveryServiceImpl();

    @Test
    void getDeliveryModes_should_return_all_values_of_enum(){
        List<DeliveryModeEnum> deliveryModes = deliveryService.getDeliveryModes();
        assertThat(deliveryModes).isEqualTo(List.of(DeliveryModeEnum.values()));
    }
}
