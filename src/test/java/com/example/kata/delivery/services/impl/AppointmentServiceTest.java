package com.example.kata.delivery.services.impl;

import com.example.kata.delivery.models.DeliveryModeEnum;
import com.example.kata.delivery.models.Slot;
import com.example.kata.delivery.services.AppointmentService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.sql.Time;
import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class AppointmentServiceTest {

    private AppointmentService appointmentService = new AppointmentServiceImpl();

    @Test
    void getAvailableSlots_should_return_only_slots_for_given_delivery() {
        //GIVEN
        Slot slot1 = new Slot();
        slot1.setDay(LocalDate.now().plusDays(1));
        slot1.setStartTime(Time.valueOf("01:00:00"));
        slot1.setEndTime(Time.valueOf("02:00:00"));
        slot1.setSlotId(Long.valueOf(1));
        slot1.setDeliveryModeEnum(DeliveryModeEnum.DELIVERY);

        Slot slot2 = new Slot();
        slot2.setDay(LocalDate.now().plusDays(1));
        slot2.setStartTime(Time.valueOf("01:00:00"));
        slot2.setEndTime(Time.valueOf("02:00:00"));
        slot2.setSlotId(Long.valueOf(2));
        slot2.setDeliveryModeEnum(DeliveryModeEnum.DELIVERY_TODAY);
        ReflectionTestUtils.setField(appointmentService, "slots", List.of(slot1, slot2));

        //WHEN
        List<Slot> result = appointmentService.getAvailableSlots(DeliveryModeEnum.DELIVERY);

        //THEN
        assertThat(result).size().isEqualTo(1);
        assertThat(result.get(0)).isEqualTo(slot1);
    }

    @Test
    void getAvailableSlots_should_return_only_slots_for_today_for_delivery_today() {
        //GIVEN
        Slot slot1 = new Slot();
        slot1.setDay(LocalDate.now().plusDays(1));
        slot1.setStartTime(Time.valueOf("01:00:00"));
        slot1.setEndTime(Time.valueOf("02:00:00"));
        slot1.setSlotId(Long.valueOf(1));
        slot1.setDeliveryModeEnum(DeliveryModeEnum.DELIVERY);

        Slot slot2 = new Slot();
        slot2.setDay(LocalDate.now().plusDays(1));
        slot2.setStartTime(Time.valueOf("01:00:00"));
        slot2.setEndTime(Time.valueOf("02:00:00"));
        slot2.setSlotId(Long.valueOf(2));
        slot2.setDeliveryModeEnum(DeliveryModeEnum.DELIVERY_TODAY);

        Slot slot3 = new Slot();
        slot3.setDay(LocalDate.now());
        slot3.setStartTime(Time.valueOf("01:00:00"));
        slot3.setEndTime(Time.valueOf("02:00:00"));
        slot3.setSlotId(Long.valueOf(3));
        slot3.setDeliveryModeEnum(DeliveryModeEnum.DELIVERY_TODAY);
        ReflectionTestUtils.setField(appointmentService, "slots", List.of(slot1, slot2, slot3));

        //WHEN
        List<Slot> result = appointmentService.getAvailableSlots(DeliveryModeEnum.DELIVERY_TODAY);

        //THEN
        assertThat(result).size().isEqualTo(1);
        assertThat(result.get(0)).isEqualTo(slot3);
    }
}
